import random

class TarotCard:
    def __init__(self, name='', description=('', ''), orientation='STANDARD'):
        self.name = name
        self.description = description
        self.orientation = orientation
    def get_card(self):
        text = '%s, Orientation: %s\n' % (self.name, self.orientation)
        if self.orientation == 'STANDARD':
            desc = self.description[0]
        else:
            desc = self.description[1]
        text += desc + '\n'
        return text
    def flip_orientation(self):
        if self.orientation != 'REVERSED':
            self.orientation = 'REVERSED'
        else:
            self.orientation = 'STANDARD'

class Spread:
    def __init__(self, draw, count=1,
                 pos_descs=[]):
        self.count = count
        self.pos_descs = pos_descs
        self.draw = draw
    def print_reading(self):
        text = ''
        for x in range(0, self.count):
            text += self.pos_descs[x] + '\n\n'
            text += self.draw[x].get_card() + '\n'
        print(text)

def parse_cards(filepath):
    cards = []
    status = 'NAME'
    name = ''
    up_desc = ''
    down_desc = ''
    with open(filepath, 'r') as inF:
        lines = inF.readlines()
        lines = ''.join(lines)
        lines = lines.split('***')
        for card in lines:
            card = card.split('\n')
            card = [x for x in card if x]


            u_i = card.index('UPRIGHT')
            r_i = card.index('REVERSED')
            name = card[0]
            upright = ', '.join(card[u_i + 1:r_i])
            rev = ', '.join(card[r_i+1:])
            cards.append(TarotCard(name=name,
                                   description=(upright, rev)))

        '''
        line='***'
        while len(line) > 0:
            line = inF.readline()
            if line.strip() == '***':

                print(name)

                cards.append(TarotCard(name, [up_desc, down_desc]))
                status = 'NAME'
                continue
            if status == 'NAME':
                name = line.strip()
                continue
            if line.startswith('UPRIGHT'):
                status = 'UPRIGHT'
                continue
            if line.startswith('REVERSED'):
                status = 'REVERSED'
                continue
            if status == 'UPRIGHT':
                up_desc = up_desc + ', ' + line.strip()
            if status == 'REVERSED':
                down_desc = down_desc + ', ' + line.strip()
        '''
    return(cards)


def main():
    cards = parse_cards('./cards.txt')
    #f = TarotCard('The Fool', ['stupid', 'smart'])

    random.shuffle(cards)

    shuffled_flipped = []
    for x in cards:
        if random.randint(0,1) == 1:
            x.flip_orientation()

        shuffled_flipped.append(x)


    past_present_future = Spread(shuffled_flipped, count=3, pos_descs=[
        'PAST', 'PRESENT', 'FUTURE'
    ])

    horseshoe = Spread(shuffled_flipped, count=7, pos_descs=[
        'THE PAST', 'THE PRESENT',
        'HIDDEN INFLUENCES', 'THE QUERENT HIM/HERSELF',
        'THE INFLUENCE OF OTHERS', 'WHAT SHOULD THE QUERENT DO?',
        'THE FINAL OUTCOME'
    ])

    horseshoe.print_reading()





main()
